# Lab4 2
#include <iostream>   
#include <algorithm>
#include <fstream>
#include <sstream>



#define double long double

    using namespace std;

    int sum[100];
    int pro[100];

    int main() {
        setlocale(LC_ALL, "Rus");
       
        std::cout << " ";
        std::ifstream in("input.txt");
        std::ifstream in2("input2.txt");
        std::ofstream out("output.txt");
        int n;
        in >> n;
        int mas[100];
        for (int i = 0; i < n; i++) {
            in >> mas[i];
        }
        for (int i = 0; i < n; i++) {
            int a = mas[i];
            pro[i] = 1;
            while (a > 0) {
                sum[i] += a % 10;
                pro[i] *= a % 10;
                a = a / 10;
            }
        }

        for (int i = 0; i < n - 1; ++i) {
            for (int j = i + 1; j < n; ++j) {
                if (sum[i] > sum[j]) {
                    swap(mas[i], mas[j]);
                    swap(sum[i], sum[j]);
                    swap(pro[i], pro[j]);
                }
                else if (sum[i] == sum[j] && pro[i] > pro[j]) {
                    swap(mas[i], mas[j]);
                    swap(sum[i], sum[j]);
                    swap(pro[i], pro[j]);
                }
                else if (sum[i] == sum[j] && pro[i] == pro[j] && mas[i] > mas[j]) {
                    swap(mas[i], mas[j]);
                    swap(sum[i], sum[j]);
                    swap(pro[i], pro[j]);
                }
            }
        }

        for (int i = 0; i < n; ++i) {
            out << mas[i] << " ";
        }
}


